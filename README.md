# 学生信息管理

#### 介绍
一个小型的学生信息管理信息系统

#### 软件架构
软件架构说明


#### 安装教程

1.  先下载数据库文件和日志的文件(StudentInfoDB.mdf和SutdentInfoDB_log.ldf)
2.  附加在Sql Server数据库上
3.  对 StudentInformation_Demo里面的appsettings.json修改Key为ConnectionStrings 内部的key为"ConStr"的值修改为自己的数据库连接字符串
4.  然后在Visual Studio 2019中使用程序包管理控制台执行update-database命令

#### 使用说明

1.  使用Visaul Studio打开项目运行即可进入该web系统
2.  登录界面的初始账号密码是 admin 123456
3.  进入系统后可以对学生信息进行增删改查的操作
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request





#### 项目展示：

登录界面：

![1603642713086](https://gitee.com/qq1149301392/student-information-management/raw/master/Demo_img/1603642713086.png)

注册界面：

![1603642788643](https://gitee.com/qq1149301392/student-information-management/raw/master/Demo_img/1603642788643.png)

学生列表页面：

![1603643983587](https://gitee.com/qq1149301392/student-information-management/raw/master/Demo_img/1603643983587.png)

添加学生页面

![1603644054183](https://gitee.com/qq1149301392/student-information-management/raw/master/Demo_img/1603644054183.png)

修改学生页面：

![1603644074937](https://gitee.com/qq1149301392/student-information-management/raw/master/Demo_img/1603644074937.png)

删除提示

![1603644123832](https://gitee.com/qq1149301392/student-information-management/raw/master/Demo_img/1603644123832.png)

![1603644137047](https://gitee.com/qq1149301392/student-information-management/raw/master/Demo_img/1603644137047.png)

分页：

![1603644333416](https://gitee.com/qq1149301392/student-information-management/raw/master/Demo_img/1603644333416.png)

![1603644341742](https://gitee.com/qq1149301392/student-information-management/raw/master/Demo_img/1603644341742.png)

查找学生：

（搜素王）

![1603644443551](https://gitee.com/qq1149301392/student-information-management/raw/master/Demo_img/1603644443551.png)

(搜索 "8")

![1603644467971](https://gitee.com/qq1149301392/student-information-management/raw/master/Demo_img/1603644467971.png)

添加专业：

(添加前的专业选项)

![1603644524726](https://gitee.com/qq1149301392/student-information-management/raw/master/Demo_img/1603644524726.png)

(添加专业)

![1603644560808](https://gitee.com/qq1149301392/student-information-management/raw/master/Demo_img/1603644560808.png)

(添加后的选项)

![1603644591532](https://gitee.com/qq1149301392/student-information-management/raw/master/Demo_img/1603644591532.png)





#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
