﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using StudentInformation_Demo.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInformation_Demo.ModelViews
{
    public class StudentViewModels
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [Display(Name = "学生学号")]
        [MaxLength(9), MinLength(9)]
        public string SNo { get; set; }
        [Required]
        [Display(Name = "学生姓名")]
        public string Name { get; set; }
        [Display(Name = "性别")]
        public GenderEnum Gender { get; set; }
        [Display(Name = "邮箱")]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [Display(Name = "电话号码")]
        public string TelPhone { get; set; }
        [Required]
        [Display(Name = "出生日期")]
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }
        [Display(Name = "图片路径")]
        public IFormFile ImgFile { get; set; }
        [Display(Name = "专业")]
        [Required(ErrorMessage = "请选择{0}")]
        public int MId { get; set; }
    }
}
