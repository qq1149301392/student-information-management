﻿using Microsoft.EntityFrameworkCore;
using StudentInformation_Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInformation_Demo.Database
{
    public class PojectDatabase : DbContext
    {
        public PojectDatabase(DbContextOptions<PojectDatabase> options):base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //设置User的UserName唯一约束
            modelBuilder.Entity<User>()
                .HasIndex(x=>x.UserName)
                .IsUnique();
            modelBuilder.Entity<Student>()
                .HasOne(x => x.Major)
                .WithMany(x => x.Students)
                .HasForeignKey(x=>x.MId);
        }
        public DbSet<Student> Students { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Major> Majors { get; set; }
    }
}
