﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInformation_Demo.Models
{
    public class Major
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "专业名称")]
        public string MojorName { get; set; }

        public ICollection<Student> Students { get; set; }
    }
}
