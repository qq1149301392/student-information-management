﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInformation_Demo.Models
{
    public class Student
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "学生学号")]
        [MaxLength(10), MinLength(10)]
        public string SNo { get; set; }
        [Required]
        [Display(Name = "学生姓名")]
        public string Name { get; set; }
        [Display(Name = "性别")]
        public GenderEnum Gender{get;set;}
        [Display(Name = "邮箱")]
        public string Email { get; set; }
        [Display(Name = "电话号码")]
        public string TelPhone { get; set; }
        [Display(Name = "出生日期")]
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }
        [Display(Name = "图片路径")]
        public string ImgPath { get; set; }
        [Display(Name = "专业")]
        public int MId { get; set; }
        public Major Major { get; set; }
    }
}
