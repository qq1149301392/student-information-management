﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInformation_Demo.Models
{
    public class PageEntity
    {
        /// <summary>
        /// 表示当前的页数(默认为1)
        /// </summary>
        private int _pageIndex = 1;
        public int PageIndex
        {
            get => _pageIndex; set
            {
                if (value == 0)
                {
                    _pageIndex = 1;
                }
                else
                {
                    _pageIndex = value;
                }
            }
        }
        /// <summary>
        /// 表示一页数的总数(默认为5)
        /// </summary>
        public int PageSize { get; set; } = 5;
        /// <summary>
        /// 表示页数的总数
        /// </summary>
        public int Count => Total / PageSize + (Total % PageSize > 0 ? 1 : 0);
        /// <summary>
        /// 表示数据的总数
        /// </summary>
        public int Total { get; set; }
        ///<summary>
        ///用于判断是否有上一页
        ///</summary>
        public bool HasPrevious => PageIndex > 1;
        ///<summary>
        ///用于判断是否有下一页
        ///</summary>
        public bool HasNext => PageIndex < Count;

    }
}
