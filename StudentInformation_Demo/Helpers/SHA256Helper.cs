﻿using StudentInformation_Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace StudentInformation_Demo.Helpers
{
    /// <summary>
    /// 给密码添加SHA256哈希算法
    /// </summary>
    public static class SHA256Helper
    {
        public static void ADDSHA256(User user)
        {
            SHA256 sHA = SHA256.Create();
            byte[] hash = sHA.ComputeHash(Encoding.UTF8.GetBytes(user.Password));
            string shaStr = BitConverter.ToString(hash);
            shaStr = shaStr.Replace("-", "");
            shaStr = shaStr.ToLower();
            user.Password = shaStr;
        }
    }
}
