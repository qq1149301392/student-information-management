﻿using StudentInformation_Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInformation_Demo.Services.Interface
{
    public interface IUserServices
    {
        User GetUser(User user);
        void AddUser(User user);
        void UpdateUser(User user);
        bool IsExistAndTrueUser(User user);
        bool Save();
    }
}
