﻿using StudentInformation_Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInformation_Demo.Services.Interface
{
    public interface IStudentServices
    {
        /// <summary>
        /// 获取一个学生信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Student GetStudent(int Id);
        /// <summary>
        /// 获取全部学生信息
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        IEnumerable<Student> GetAllStudent(PageEntity page);
        /// <summary>
        /// 添加一个学生信息
        /// </summary>
        /// <param name="model"></param>
        void AddStudent(Student model);
        /// <summary>
        /// 删除一个学生信息
        /// </summary>
        /// <param name="Id"></param>
        void DeleteStudent(int Id);
        /// <summary>
        /// 修改一个学生信息
        /// </summary>
        /// <param name="model"></param>
        void UpdateStudent(Student model);
        /// <summary>
        /// 通过模糊查询来查找一个学生
        /// </summary>
        /// <param name="queryStr"></param>
        /// <returns></returns>
        IEnumerable<Student> SelectStudent(string queryStr);
        /// <summary>
        /// 在执行数据库增删改时 要执行的操作
        /// </summary>
        /// <returns></returns>
        bool Save();
        /// <summary>
        /// 获取所有专业列表
        /// </summary>
        /// <returns></returns>
        IEnumerable<Major> GetAllMajors();
        /// <summary>
        /// 获取单个专业的数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Major GetMajor(int Id);
        /// <summary>
        /// 添加一个专业的数据
        /// </summary>
        /// <param name="major"></param>
        void AddMajor(Major major);
    }
}
