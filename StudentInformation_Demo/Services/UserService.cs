﻿using StudentInformation_Demo.Database;
using StudentInformation_Demo.Helpers;
using StudentInformation_Demo.Models;
using StudentInformation_Demo.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInformation_Demo.Services
{
    public class UserService : IUserServices
    {
        private readonly PojectDatabase database;

        public UserService(PojectDatabase database)
        {
            this.database = database;
        }
        public void AddUser(User user)
        {
            SHA256Helper.ADDSHA256(user);
            database.Users.Add(user);
        }

        public User GetUser(User user)
        {
            return database.Users.FirstOrDefault(x => x.UserName == user.UserName);
        }

        public bool IsExistAndTrueUser(User user)
        {
            SHA256Helper.ADDSHA256(user);
            var model = database.Users.FirstOrDefault(x => x.UserName == user.UserName);
            if (model == null)
            {
                return false;
            }
            else if (model.Password.Equals(user.Password))
            {
                return true;
            }
            return false;
        }

        public bool Save()
        {
            return database.SaveChanges() > 0;
        }

        public void UpdateUser(User user)
        {
            SHA256Helper.ADDSHA256(user);
            database.Users.Update(user);
        }
    }
}
