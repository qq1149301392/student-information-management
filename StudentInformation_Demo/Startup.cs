using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StudentInformation_Demo.Authorization;
using StudentInformation_Demo.Database;
using StudentInformation_Demo.Services;
using StudentInformation_Demo.Services.Interface;

namespace StudentInformation_Demo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //添加过滤器
            services.AddControllersWithViews(option =>
            {
                option.Filters.Add(typeof(AuthenFilterAttribute));
            });
            //配置authorrize
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme
            //    b =>
            //{
            //    b.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            //    b.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            //    b.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            //})
            ).AddCookie(CookieAuthenticationDefaults.AuthenticationScheme,b =>
            {
                b.LoginPath = new PathString("/Account/Login");
                //b.Cookie.Name = "msc_auth_name";
                //b.Cookie.Path = "/";
                b.SlidingExpiration = true;
                b.Cookie.HttpOnly = true;
                b.ExpireTimeSpan = TimeSpan.FromMinutes(10);
            });
            services.AddControllersWithViews();
            services.AddDbContext<PojectDatabase>(options=> 
            {
                options.UseSqlServer(Configuration.GetConnectionString("ConStr"));
            });
            services.AddScoped<IStudentServices, StudentServices>();
            services.AddScoped<IUserServices, UserService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            //UseAuthentication必须在UseAuthorization之前
            app.UseAuthentication();
            app.UseAuthorization();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Account}/{action=Login}/{id?}");
            });
        }
    }
}
